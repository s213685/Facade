Feature: Merchant Service
  Scenario: Merchant registration success
    Given there is a merchant account with firstName "thomas", lastName "spyrou", cpr "3", type "1", bankAccountID "234"
    When the merchant is being registered
    Then the event "AccountRegistrationRequested" is published for the merchant
    When the merchantId is received from account management for the merchant
    Then the merchant is created


  Scenario: Merchant registration fail
    Given there is a merchant account with firstName "thomas", lastName "spyrou", cpr "4", type "1", bankAccountID "234"
    When the merchant is being registered
    Then the event "AccountRegistrationRequested" is published for the merchant
    When An error is received from account management for the merchant
    Then the merchant is not created
