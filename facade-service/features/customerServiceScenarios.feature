Feature: Customer Service
  Scenario: Customer registration success
    Given there is a customer with firstName "thomas", lastName "spyrou", cpr "1", type "0", bankAccountID "234"
    When the customer is being registered
    Then the event "AccountRegistrationRequested" is published
    When the customerId is received from account management
    Then the customer is created


  Scenario: Customer registration fail
    Given there is a customer with firstName "thomas", lastName "spyrou", cpr "2", type "0", bankAccountID "234"
    When the customer is being registered
    Then the event "AccountRegistrationRequested" is published
    When An error is received from account management
    Then the customer is not created

  Scenario: Customer registration interleaving requests
    Given there is a customer with firstName "thomas", lastName "spyrou", cpr "3", type "0", bankAccountID "234"
    When the customer is being registered
    Then the "AccountRegistrationRequested" event is published for the first customer
    Given another customer with with firstName "nikos", lastName "karavasilis", cpr "4", type "0", bankAccountID "123"
    When the second customer is being registered
    Then the "AccountRegistrationRequested" event is published for the second customer
    When the AccountRequestCompleted event is received for the second customer
    Then the second customer is registered and his id is set
    When the AccountRequestFailed event is received for the first customer
    Then the first customer is not registered and his id is not set

