package controllers;
/*
@authors:
Karavasilis Nikos s213685
Spyrou Thomas s213161
 */

import java.util.concurrent.ConcurrentHashMap;
import dto.ReturnAccountInfo;
import entities.Account;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import service.AccountService;
import utils.EventTypes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import static org.junit.Assert.assertNotNull;


public class CustomerServiceScenarios {
    private Map<String, CompletableFuture<Event>> publishedEvents = new ConcurrentHashMap<>();
    private Map<String, CompletableFuture<ReturnAccountInfo>> accRequests;

    //private Account accountSent;// = new Account();

    private MessageQueue queue = new MessageQueue() {

        @Override
        public void publish(Event event) {
            var account = event.getArgument(0, Account.class);
            publishedEvents.get(account.getCprNumber()).complete(event);
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };

    private AccountService accountService = new AccountService(queue);
    private Account customer;
    private Account customer2;
    private Account customerSent;
    private Account customerSent2;
    private CompletableFuture<ReturnAccountInfo> accountCompletableFuture = new CompletableFuture<>();
    private CompletableFuture<ReturnAccountInfo> accountCompletableFuture2 = new CompletableFuture<>();
    private Map<Account, String> correlationIds = new HashMap<>();

    @Before
    public void before(){

    }

    @Given("there is a customer with firstName {string}, lastName {string}, cpr {string}, type {string}, bankAccountID {string}")
    public void thereIsACustomerWithFirstNameLastNameCprTypeBankAccountID(String firstName, String lastName, String cpr,
                                                                          String type, String bankAccountID) {
        customer = new Account();
        customer.setLastName(lastName);
        customer.setFirstName(firstName);
        customer.setType(type);
        customer.setBankAccountId(bankAccountID);
        customer.setCprNumber(cpr);
        publishedEvents.put(customer.getCprNumber(), new CompletableFuture<>());
        assertNotNull(customer);
    }

    @When("the customer is being registered")
    public void theCustomerIsBeingRegistered() {
        new Thread(() -> {
            try {
                var result = (ReturnAccountInfo) accountService.registerAccount(customer);
                accountCompletableFuture.complete(result); //wait
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Given("another customer with with firstName {string}, lastName {string}, cpr {string}, type {string}, bankAccountID {string}")
    public void anotherCustomerWithWithFirstNameLastNameCprTypeBankAccountID(String firstName, String lastName, String cpr,
                                                                             String type, String bankAccountID) {
        customer2 = new Account();
        customer2.setLastName(lastName);
        customer2.setFirstName(firstName);
        customer2.setType(type);
        customer2.setBankAccountId(bankAccountID);
        customer2.setCprNumber(cpr);
        publishedEvents.put(customer2.getCprNumber(), new CompletableFuture<>());
        assertNotNull(customer2);
    }

    @When("the second customer is being registered")
    public void theSecondCustomerIsBeingRegistered() {
        new Thread(() -> {
            try {
                var result = (ReturnAccountInfo) accountService.registerAccount(customer2);
                accountCompletableFuture2.complete(result); //wait
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

    }

    @Then("the event {string} is published")
    public void theEventIsPublished(String eventType) {
        Event event = publishedEvents.get(customer.getCprNumber()).join();
        assertEquals(eventType, event.getType());
        customerSent = new Account();
        customerSent = event.getArgument(0, Account.class);
        var correlationId = event.getCorrID();
        correlationIds.put(customerSent, correlationId);
    }

    @Then("the {string} event is published for the first customer")
    public void theEventIsPublishedForTheFirstCustomer(String eventType) {
        Event event = publishedEvents.get(customer.getCprNumber()).join();
        assertEquals(eventType, event.getType());
        customerSent = new Account();
        customerSent = event.getArgument(0, Account.class);
        var correlationId = event.getCorrID();
        correlationIds.put(customerSent, correlationId);
    }


    @Then("the {string} event is published for the second customer")
    public void theEventIsPublishedForTheSecondCustomer(String eventType) {
        Event event = publishedEvents.get(customer2.getCprNumber()).join();
        assertEquals(eventType, event.getType());
        customerSent2 = new Account();
        customerSent2 = event.getArgument(0, Account.class);
        var correlationId = event.getCorrID();
        correlationIds.put(customerSent2, correlationId);
    }

    @When("the AccountRequestCompleted event is received for the second customer")
    public void theAccountRequestCompletedEventIsReceivedForTheSecondCustomer() {
        accountService.handleAccountRegistrationCompleted(
                new Event(EventTypes.ACCOUNT_REGISTRATION_COMPLETED, correlationIds.get(customerSent2),
                        new Object[] {UUID.randomUUID().toString()}));
    }

    @Then("the second customer is registered and his id is set")
    public void theSecondCustomerIsRegisteredAndHisIdIsSet() {
        var st2 = accountCompletableFuture2.join();
        assertNotEquals("",st2.getAccountId());
    }

    @When("the AccountRequestFailed event is received for the first customer")
    public void theAccountRequestFailedEventIsReceivedForTheFirstCustomer() {
        accountService.handleAccountRegistrationFailed(
                new Event(EventTypes.ACCOUNT_REGISTRATION_FAILED, correlationIds.get(customerSent),
                        new Object[] {"Failed"}));
    }

    @Then("the first customer is not registered and his id is not set")
    public void theFirstCustomerIsNotRegisteredAndHisIdIsNotSet() {
        var st1 = accountCompletableFuture.join();
        assertEquals("",st1.getAccountId());
    }

    @When("the customerId is received from account management")
    public void theCustomerIdIsReceivedFromAccountManagement(){
        accountService.handleAccountRegistrationCompleted(
                new Event(EventTypes.ACCOUNT_REGISTRATION_COMPLETED, correlationIds.get(customerSent),
                        new Object[] {UUID.randomUUID().toString()}));
    }

    @Then("the customer is created")
    public void theCustomerIsCreated() {
        assertNotNull(accountCompletableFuture.join().getAccountId());
    }

    @When("An error is received from account management")
    public void anErrorIsReceivedFromAccountManagement()  {
        accountService.handleAccountRegistrationFailed(
                new Event(EventTypes.ACCOUNT_REGISTRATION_FAILED, correlationIds.get(customerSent),
                        new Object[] {"Failed"}));

    }

    @Then("the customer is not created")
    public void theCustomerIsNotCreated() {
        assertNotNull(accountCompletableFuture.join().getErrorMessage());

    }

}

