/*
@authors:
Karavasilis Nikos s213685
Spyrou Thomas s213161
 */

package controllers;

import dto.ReturnAccountInfo;
import entities.Account;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import service.AccountService;
import utils.EventTypes;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MerchantServiceScenarios {
    private Map<String, CompletableFuture<Event>> publishedEvents = new ConcurrentHashMap<>();
    private Map<String, CompletableFuture<ReturnAccountInfo>> accRequests;

    //private Account accountSent;// = new Account();

    private MessageQueue queue = new MessageQueue() {

        @Override
        public void publish(Event event) {
            var account = event.getArgument(0, Account.class);
            publishedEvents.get(account.getCprNumber()).complete(event);
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }

    };

    private AccountService accountService = new AccountService(queue);
    private Account merchant; //= new Account();
    private Account merchantSent;
    private CompletableFuture<ReturnAccountInfo> accountCompletableFuture = new CompletableFuture<>();
    private Map<Account, String> correlationIds = new HashMap<>();

    @Before
    public void before(){

    }

    @Given("there is a merchant account with firstName {string}, lastName {string}, cpr {string}, type {string}, bankAccountID {string}")
    public void thereIsAMerchantAccountWithFirstNameLastNameCprTypeBankAccountID(String firstName, String lastName,
                                                                                 String cpr, String type, String bankAccountID) {
        merchant = new Account();
        merchant.setLastName(lastName);
        merchant.setFirstName(firstName);
        merchant.setType(type);
        merchant.setBankAccountId(bankAccountID);
        merchant.setCprNumber(cpr);
        publishedEvents.put(merchant.getCprNumber(), new CompletableFuture<>());
        assertNotNull(merchant);
    }

    @When("the merchant is being registered")
    public void theCustomerIsBeingRegistered() {
        Thread t = new Thread(() -> {
            try {
                var result = (ReturnAccountInfo) accountService.registerAccount(merchant);
                accountCompletableFuture.complete(result); //wait
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        t.start();
    }

    @Then("the event {string} is published for the merchant")
    public void theEventIsPublishedForTheMerchant(String eventType) {
        Event event = publishedEvents.get(merchant.getCprNumber()).join();
        assertEquals(eventType, event.getType());
        merchantSent = new Account();
        merchantSent = event.getArgument(0, Account.class);
        var correlationId = event.getCorrID();
        correlationIds.put(merchantSent, correlationId);
    }

    @When("the merchantId is received from account management for the merchant")
    public void theMerchantIdIsReceivedFromAccountManagementForTheMerchant(){
        accountService.handleAccountRegistrationCompleted(
                new Event(EventTypes.ACCOUNT_REGISTRATION_COMPLETED, correlationIds.get(merchantSent),
                        new Object[] {UUID.randomUUID().toString()}));
    }

    @Then("the merchant is created")
    public void theCustomerIsCreated() {
        assertNotNull(accountCompletableFuture.join().getAccountId());

    }

    @When("An error is received from account management for the merchant")
    public void anErrorIsReceivedFromAccountManagementForTheMerchant()  {
        accountService.handleAccountRegistrationFailed(
                new Event(EventTypes.ACCOUNT_REGISTRATION_FAILED, correlationIds.get(merchantSent),
                        new Object[] {"Failed"}));

    }

    @Then("the merchant is not created")
    public void theCustomerIsNotCreated() {
        assertNotNull(accountCompletableFuture.join().getErrorMessage());

    }

}

